import entity.Document;
import entity.Permission;
import entity.User;
import service.DocumentService;
import service.DocumentServiceImpl;

import java.util.HashMap;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        User user1=new User("Saikat");
        User user2 = new User("Ayan");

        DocumentService documentService =new DocumentServiceImpl();

        Document doc1=documentService.createDocument(user1.getUsername(),"first Doc","Hello world");
        Document doc2=documentService.createDocument(user2.getUsername(),"second Doc","Hello again");

        String documentContentBefore=documentService.readDocument("first Doc",user2.getUsername());
//        System.out.println(documentContentBefore);


        HashMap<String,Set<Permission>> permissions=new HashMap<>();
        permissions.put(user2.getUsername(), Set.of(Permission.READ));
        documentService.shareDocument("first Doc","Saikat",permissions);
//        System.out.println(doc1.getMetaData());

        String documentContentAfter=documentService.readDocument("first Doc",user2.getUsername());
//        System.out.println(documentContentAfter);

        documentService.editDocument("first Doc", user2.getUsername(), "Changing content");

        HashMap<String,Set<Permission>> newPermissions=new HashMap<>();
        newPermissions.put(user2.getUsername(), Set.of(Permission.EDIT));
        documentService.shareDocument("first Doc","Saikat",newPermissions);

        documentService.editDocument("first Doc", user2.getUsername(), "Changing content");

        String documentContentAfterEdit=documentService.readDocument("first Doc",user2.getUsername());
        System.out.println(documentContentAfterEdit);

        System.out.println(doc1.getMetaData());

        documentService.deleteDocument("first Doc", user1.getUsername());
        String documentContentAfterDelete=documentService.readDocument("first Doc",user1.getUsername());
        System.out.println(documentContentAfterDelete);

    }
}
