package Repository;
import entity.Document;

import java.util.HashMap;
/*
DocumentRepository:
	Map<documentName, Document>

	-getDocument(documentName)
	-addDocument(documentName, Document )
    -editDocument(documentName, Document) (add later)
	-deleteDocument(documentName)
 */



public class DocumentRepository {
    HashMap<String, Document> documents=new HashMap<>();

    public Document getDocumentByName(String documentName){
        return documents.get(documentName);
    }

    public HashMap<String, Document> getDocuments() {
        return documents;
    }

    public Document addDocument(Document document){
        documents.put(document.getName(), document);
        return document;
    }
//
//    public Document updateDocument(String documentName, Document document){
//        return null;
//    }

    public void deleteDocument(String documentName){
        documents.remove(documentName);
    }
}
