package entity;


/*
Document:
	Name
	Content
	Meta
		Map<username,permission>
		Owner : user

 */

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Document {
    private String name;
    private String content;

    private MetaData metaData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public Document(String name, String ownerUsername, String content) {
        this.name = name;
        this.content = content;
        HashMap<String, Set<Permission>> permissions=new HashMap<>();
        permissions.put(ownerUsername,Set.of(Permission.READ, Permission.EDIT, Permission.DELETE));

        this.metaData=new MetaData(ownerUsername, permissions);
    }
}
