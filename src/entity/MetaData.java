package entity;

/*
	Meta
		Map<username,permission>
		Owner : user
 */

import java.util.HashMap;
import java.util.Set;


public class MetaData {
    String ownerUserName;
    HashMap<String, Set<Permission>> permissions;

    public MetaData(String ownerUserName, HashMap<String, Set<Permission>> permissions) {
        this.ownerUserName = ownerUserName;
        this.permissions = permissions;
    }

    @Override
    public String toString() {
        StringBuilder output=new StringBuilder();

        for(String key:permissions.keySet()){
            output.append(key).append(":").append(permissions.get(key)).append("\n");
        }
        return output.toString();
    }

    public String getOwnerUserName() {
        return ownerUserName;
    }

    public HashMap<String, Set<Permission>> getPermissions() {
        return permissions;
    }

    public void setPermissions(HashMap<String, Set<Permission>> permissions) {
        this.permissions = permissions;
    }


}
