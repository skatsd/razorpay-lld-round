package entity;

/*
Permission:
	READ
	EDIT
	DELETE (owner)

 */

public enum Permission {
    READ, EDIT, DELETE
}
