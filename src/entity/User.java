package entity;

/*
User:
	Username: String
 */

public class User {
    private String username;

    public User(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
    
}
