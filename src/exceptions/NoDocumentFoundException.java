package exceptions;

public class NoDocumentFoundException extends  RuntimeException{
    public NoDocumentFoundException(String message) {
        super(message);
    }
}
