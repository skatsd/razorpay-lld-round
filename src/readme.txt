Design and implement a Simple document Service where users can create documents and read the same.
A document has a name and associated string content. Document=> <name{string}, content{string}>

1. All documents are private when created.
2. Owners of documents can {grant} {read OR edit} access to other users
3.Only the owner can delete a document
4. Username will be just a string. Every action like create/read/edit/delete must be made on behalf of a user
—


Entity:

Document:
	Name
	Content
	Meta
		Map<username,permission>
		Owner : user

User:
	Username: String

Permission:
	READ
	EDIT
	DELETE (owner)

Service:
	-DocumentService
		-createDocument(userName,documentName,content)
		-shareDocument(documentName, ownerUsername, HashMap<username, permission>)
		-deleteDocument(documentId, ownerUsername)

		-readDocument(documentName, userName)
		-editDocument(documentName, userName, content)

DocumentRepository:
	Map<documentName, Document>

	-getDocument(documentName)
	-addDocument(documentName, Document )
    -editDocument(documentName, Document)
	-deleteDocument(documentName)

Bonus:
	-updateDocument





