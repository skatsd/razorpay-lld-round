package service;

/*
Service:
	-DocumentService
		-createDocument(userName,documentName,content)
		-shareDocument(documentName, ownerUsername, HashMap<username, permission>)
		-deleteDocument(documentId, ownerUsername)

		-readDocument(documentName, userName)
		-editDocument(documentName, userName, content)
 */

import entity.Document;
import entity.Permission;

import java.util.HashMap;
import java.util.Set;

public interface DocumentService {
    Document createDocument(String userName, String documentName, String content);
    void shareDocument(String documentName, String ownerUsername, HashMap<String, Set<Permission>> permissions);
    void deleteDocument(String documentName,String user);

    String readDocument(String documentName, String userName);
    Document editDocument(String documentName, String userName, String content);
}
