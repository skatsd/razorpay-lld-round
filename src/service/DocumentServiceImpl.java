package service;

import Repository.DocumentRepository;
import entity.Document;
import entity.Permission;
import exceptions.DocumentAlreadyExistsException;
import exceptions.NoDocumentFoundException;
import exceptions.NotOwnerException;

import java.util.HashMap;
import java.util.Set;

public class DocumentServiceImpl implements  DocumentService{

    private final DocumentRepository documentRepository = new DocumentRepository();

    private boolean documentAlreadyExists(String documentName){
        if(documentRepository.getDocumentByName(documentName)!=null){
            return true;
        }
       return false;
    }

    private boolean isOwner(String documentName, String username){
        if(documentAlreadyExists(documentName)){
            Document document =documentRepository.getDocumentByName(documentName);

            return document.getMetaData().getOwnerUserName().equals(username);
        }

        return false;
    }

    @Override
    public Document createDocument(String userName, String documentName, String content) {

        if(!documentAlreadyExists(documentName)){
            Document document = new Document(documentName, userName, content);
            documentRepository.addDocument(document);
            return document;
        }else{
            throw new DocumentAlreadyExistsException("Document already exists!!!");
        }
    }

    @Override
    public void shareDocument(String documentName, String ownerUsername, HashMap<String, Set<Permission>> permissions) {
        if(documentAlreadyExists(documentName)){
            if(isOwner(documentName,ownerUsername)) {



                Document document = documentRepository.getDocumentByName(documentName);
                HashMap<String, Set<Permission>> existingPermissions = document.getMetaData().getPermissions();
                for (String userName : permissions.keySet()) {
                    existingPermissions.put(userName, permissions.get(userName));
                }
            }else{
                throw new NotOwnerException("User is not owner!!!");
            }
        }else{
            throw new NoDocumentFoundException("No Document found!!!");
        }
    }

    @Override
    public void deleteDocument(String documentName, String user) {
        if(documentAlreadyExists(documentName)) {
            if(isOwner(documentName, user)) {
                documentRepository.deleteDocument(documentName);
            }else{
                throw new NotOwnerException("User is not owner!!!");
            }
        }else{
            throw new NoDocumentFoundException("No Document found!!!");
        }
    }

    @Override
    public String readDocument(String documentName, String userName) {
        if(documentAlreadyExists(documentName)) {
            Document document=documentRepository.getDocumentByName(documentName);
            HashMap<String, Set<Permission>> permissions = document.getMetaData().getPermissions();

            if(permissions.containsKey(userName)){
                Set<Permission> userPermissions=permissions.get(userName);
                if(userPermissions.contains(Permission.READ)){
                    return document.getContent();
                }
            }

        }else{
            throw new NoDocumentFoundException("No Document found!!!");
        }
        return "";
    }

    @Override
    public Document editDocument(String documentName, String userName, String content) {
        if(documentAlreadyExists(documentName)) {
            Document document=documentRepository.getDocumentByName(documentName);

            HashMap<String, Set<Permission>> permissions = document.getMetaData().getPermissions();

            if(permissions.containsKey(userName)){
                Set<Permission> userPermissions=permissions.get(userName);
                if(userPermissions.contains(Permission.EDIT)){
                    document.setContent(content);
                }
            }
            return document;
        }else{
            throw new NoDocumentFoundException("No Document found!!!");
        }
    }
}
